# Dominio y Empresa

  

**Nombre de la empresa:** EnjoyCream

**Motivo**: Venta de helados

Primero tenemos que crear nuestro dominio. En mi caso utilizo **Ionos1and1**. Facilitaremos una **tarjeta de crédito** o **PayPal** para el proceso, que le resulta la mar de intuitivo.

Una vez tenemos el dominio nos dirigimos a **Google Sites** para crear nuestra web. Seleccionamos una de ejemplo para ahorrar tiempo. La he llamado enjoycream todo junto.

Mi empresa trata sobre la venta de helados y se llama **EnjoyCream**.

Una vez lo tenemos preparado vamos a Ionos y añadimos el enlace de Google Sites para que al escribir nuestro dominio en el navegador nos redireccione directamente a nuestro gsite. Esto nos ahorra tiempo y nos aseguramos que nuestra web sea segura con el https. Mi dominio se llama **sgallego-empresa** y puedes acceder desde [aquí](https://sites.google.com/view/enjoycream) 

Una vez tenemos la web pasamos a redireccionar los correos. Vamos a la configuración de gmail y seguimos el paso a paso que hay en el documento para llegar a una ventana donde nos pedirá una dirección de correo y otra de respuesta. En la de respuesta escribimos la que pusimos en **Ionos**.

Creamos un **grupo** que se llame EnjoyCream. En este grupo añadimos las dos cuentas que hemos creado para el jefe y el técnico. El jefe será el administrador y nuestra cuenta el propietario.

Con este grupo nos permitirá enviar correos a todos los integrantes con una sola dirección, lo cual nos ahorra tiempo y cualquier tipo de error.

Creamos un **Trello** junto con otro usuario. En este Trello podremos podremos adjuntar todas las tareas correspondientes a cada departamento o trabajador.

Con **genially** podremos crear presentaciones de forma profesional e interactiva para que sea mas vistoso de cara al público. Puedes ver un concepto de presentación de nuestra empresa [aquí](https://view.genial.ly/5f981a14a536360d0def4081/presentation-genially-sin-titulo).

















                                                                                               d
                                                                                               dd
                                                                                               d d
                                                                                               d  d
                                                                                              dd   d
                                                                                              d     d
                                                                                            dd        dd
                                                                                       dd dd           ddd
                                                                                   ddd                   ddd
                                                                                 d d                       dddd
                                                                              d d                              dd
                                                                             d       d ddd ddddd                 dd
                                                                            ddd dd d            ddddddddddddddd   dd
                                                                        dddd                                   ddddd
                                                                      ddd                                          d ddd
                                                                     dd                                                 dd
                                                                                                                          dd
                                                              ddddddddddd dd                                               dd
                                                            dd              ddddddddddddd d                                 dd
                                                          dd                              d ddddd ddd dddd dddddddd ddddddd
                                                          d                                                                d dd
                                                        d                                                                     dddd
                                                       d                                                                          dd
                                                       d                                                                           dd
                                                       d                                                                            dd
                                                       d                                                                             d
                                                       dd                                                                            d
                                                        d                                                                           dd
                                                        ddd                                                                       ddd
                                                          ddd     dddddddddddddddd d d d dd dddddd d d d d d  d d  d d  d  ddddddd
                                                             ddddd                                                           d
                                                                 d                                                           d
                                                                   d                                                        dd
                                                                                                                           d
                                                                                                                           d
                                                                    d                                                     d
                                                                                                                          d
                                                                      d                                                  d
                                                                                                                        d
                                                                        d
                                                                                                                      d
                                                                         d                                            d
                                                                                                                     d
                                                                          d                                         d
                                                                           d                                        d
                                                                                                                   d
                                                                            d                                    dd
                                                                                                                 d
                                                                             d                                  d
                                                                             d                                 d
                                                                              d                               d
                                                                              d
                                                                                                              d
                                                                               d                              d
                                                                                d                            d
                                                                                d                           d
                                                                                 d                         d
                                                                                 d                        d
                                                                                 d
                                                                                  d                      d
                                                                                   d                     d
                                                                                   dd                  d
                                                                                    d                 d
                                                                                     d             d
                                                                                      d      d  d
                                                                                       ddddd















